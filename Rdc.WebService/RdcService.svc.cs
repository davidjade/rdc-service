﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Rdc.WebService.Contracts;
using Rdc.Wrapper;

namespace Rdc.WebService
{    
    public class RdcService : IRdcService
    {
        private readonly ISignatureRepository _signatureRepository = new SimpleSignatureRepository();
        public SignatureManifest GetSignatureManifest(string fileName)
        {
            SignatureManifest manifest;
            IList<SignatureInfo> signatures;

            using (var stream = File.OpenRead(fileName))
            {
                if (_signatureRepository.GetLastUpdate(fileName) == null)
                {
                    using (var rdcServices = new SigGenerator(_signatureRepository))
                    {
                        var generatedSignatures = rdcServices.GenerateSignatures(stream);
                        if (generatedSignatures.Count < 1)
                        {
                            throw new RdcException("Failed to generate the signatures.");
                        }
                        _signatureRepository.AssingToFileName(generatedSignatures, fileName);
                    }
                }
                signatures = _signatureRepository.GetByFileName(fileName).ToList();
                manifest =
                    new SignatureManifest
                        {

                            FileName = fileName,
                            FileLength = stream.Length,
                            Signatures = signatures.Select(ToSignature).ToList()
                        };
            }
            return manifest;
        }

        private static Signature ToSignature(SignatureInfo signatureInfo)
        {
            var result = new Signature()
            {
                Length = signatureInfo.Length,
                Name = signatureInfo.Name
            };
            return result;
        }

        public byte[] GetSignatureContent(string name, int offset, int length)
        {
            using (var sigContent = _signatureRepository.GetContentForReading(name))
            {
                return ReadFully(sigContent, offset, length);
            }
        }

        public byte[] TransferDataBlock(string fileName, int offset, int length)
        {
            using (var file = File.OpenRead(fileName))
            {
                return ReadFully(file, offset, length);
            }
        }

        public static byte[] ReadFully(Stream input, long offset, long length)
        {
            input = new NarrowedStream(input, offset, offset + length - 1);
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
