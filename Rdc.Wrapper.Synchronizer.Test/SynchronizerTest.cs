﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rdc.Utils.IO;

namespace Rdc.Wrapper.Synchronization.Test
{
    [TestFixture]
    public class SynchronizerTest
    {
        private readonly ISignatureRepository _signatureRepository = new SimpleSignatureRepository();

        [TestFixtureSetUp]
        public void Init()
        {

            using (Stream file = File.Create("source.bin"))
            {
                new RandomStream(1024 * 1024 * 30, 1).CopyTo(file);
            }

            using (Stream file = File.Create("seed.bin"))
            {
                new RandomlyModifiedStream(new RandomStream(1024 * 1024 * 30, 1), 0.001, 1).CopyTo(file);
            }
        }

        [Test]
        public void CopySourceSignatures_check()
        {            
            var tested = new Synchronizer(_signatureRepository);
            using (Stream file = File.OpenRead("source.bin"))
            {
                var result = tested.CopySourceSignatures(((FileStream)file).Name);
                Assert.NotNull(result);
            }
        }

        [Test]
        public void GenerateNeedList_check()
        {
            var tested = new Synchronizer(_signatureRepository);

            IList<SignatureInfo> sourceSignatures;
            IList<SignatureInfo> seedSignatures;
            using (var sigGenerator = new SigGenerator(_signatureRepository))
            {
                using (FileStream source = File.OpenRead("source.bin"), seed = File.OpenRead("seed.bin"))
                {
                    sourceSignatures = sigGenerator.GenerateSignatures(source);
                    seedSignatures = sigGenerator.GenerateSignatures(seed);
                }
            }
            var pairs = seedSignatures.Zip(sourceSignatures, (seed, source) => new Tuple<SignatureInfo, SignatureInfo>(seed, source));
            var result = tested.GenerateNeedList(pairs);
            Assert.NotNull(result);
        }

        [Test]
        public void Check_if_source_file_is_the_same_as_target()
        {
            if (File.Exists("output.bin"))
            {
                File.Delete("output.bin");
            }            
            var tested = new Synchronizer(_signatureRepository);
            var sourcePath = Path.Combine(Directory.GetCurrentDirectory(), "source.bin");
            tested.Process(sourcePath, "seed.bin", "output.bin");
            Assert.IsTrue(File.Exists("output.bin"));
            FileAssert.AreEqual(sourcePath, "output.bin");
        }
    }
}
